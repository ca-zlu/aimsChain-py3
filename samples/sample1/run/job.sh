#!/bin/bash
rm -r climbing_forces.log forces.log growing_forces.log iterations paths optimized grownstring

export OMP_NUM_THREADS=1
here=`pwd`
module load intel-parallel-studio/cluster.2020.0 
ulimit -s unlimited
echo $here
export PYTHONPATH=$PYTHONPATH:$here/../../../
echo $PYTHONPATH

python3 runchain.py

../getpath.sh

