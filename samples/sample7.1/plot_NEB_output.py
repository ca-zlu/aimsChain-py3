import os, sys, glob
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

os.chdir('optimized')                                                      # go to optimized directory
files = [x for x in glob.glob('image*/*.out')]                             # reads all the prepared images
flen = len(files)                                                          # amount of calculated images - together with startpoint & endpoint
energies = np.zeros((flen,2))                                              # creating array to store energies;
for iff,f in enumerate(files):
    print ("image no.", iff+1)                                             # just to find out, where we are
    for line in open(f):                                                   # go through file and extract energy
           if '| Total energy of the DFT / Hartree-Fock s.c.f. calculation      :' in line:
               energies[iff,0]=float(iff+1)
               energies[iff,1]=float(line.split()[-2])
               print (float(line.split()[-2]))
               break

os.chdir('..')                                                             # go to the main directory

f= open("NEB_energies.txt",'w')
for i in range(flen):
    print (energies[i,0],energies[i,1],file=f)
f.close()

print ("plotting:")

fig=plt.figure(0)                                                          # create figure
ax=fig.add_subplot(111)                                                    # create axes
ax.plot(energies[:,0],energies[:,1],'o-',c='b',label=None )                # plot NEB image vs. energy
ax.set_xlabel('image number',size=20)                                      # x-label
ax.set_ylabel('Energy [eV]',size=20)                                       # y-label
#fig.show()
fig.savefig('Results_energy.pdf')

print ("Done")


